#!/bin/bash

while [ -n "$1" ]; do 
    case "$1" in

    -a) echo "-a option was given to Me" ;; 

    -b) echo "-b option was shown to this script" ;;

    -john) echo "This is the best option. " ;;

    --) 
       shift # Separate out the parameters to my shell script. 
       
       break
       ;;  # Exits this loop and looks now for parameters. 

    esac

    shift

done

total=1 

for param in $@; do 

    echo "#$total: $param"
    total=$((total + 1))

done
