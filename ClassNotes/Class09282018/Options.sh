#!/bin/bash

PS3='Please give me an option: '
options=("Option 1" "Option 2" "Option 3" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Option 1") 
             echo "This was my first option"
             ;;
        "Option 2")
             echo "The second option is better"
             ;;
        "Option 3") 
             echo "Here Darth Vader will come to your house and list all your files:"
             ls
             ;;
        "Quit")
             break
             ;;
        *) echo "Choose something listed you fool!"
    esac
done
