#!/bin/bash

# Hopefully we will read some parameters for this script from the command line. 

echo "To Use: ./ArgmmentsExample.sh Thing1 Thing2 Thing3"

POSPAR1="$1"
POSPAR2="$2"
POSPAR3="$3"

echo "$1 is the first position paramter. \$1."
echo "$2 is the second position paramter. \$2."
echo "$3 is the third positon paramter. \$3."
echo 
echo "The total number of paramters is given by $#."
